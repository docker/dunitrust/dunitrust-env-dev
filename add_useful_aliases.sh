#!/bin/bash

echo '# Alias definitions.' >> .bashrc
echo '. ~/.bash_aliases' >> .bashrc

echo '# Useful aliases for the Dunitrust project' > .bash_aliases

echo 'alias cb="cargo fmt -- --check && cargo build"' >> .bash_aliases
echo 'alias cbr="cargo fmt -- --check && cargo build --release"' >> .bash_aliases
echo 'alias cbrf="cargo fmt -- --check && cargo build --release --manifest-path bin/dunitrust-server/Cargo.toml --features ssl"' >> .bash_aliases
echo 'alias cc="cargo fmt -- --check && cargo check"' >> .bash_aliases
echo 'alias cp="cargo clippy"' >> .bash_aliases
echo 'alias cr="cargo run --release --"' >> .bash_aliases
echo 'alias fmt="cargo fmt"' >> .bash_aliases
echo 'alias tc="cargo fmt && cargo test --package"' >> .bash_aliases
echo 'alias ta="cargo fmt && cargo test --all"' >> .bash_aliases
echo 'alias tac="cargo fmt && cargo tarpaulin --all -i -o Html -- --skip test_connection_negociation"' >> .bash_aliases
