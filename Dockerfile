FROM rust:slim
MAINTAINER elois <elois@duniter.org>
LABEL version="0.1.3"
LABEL description="Minimal environment for a Dunitrust developer"

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

# Add libraries needed to compile dunitrust
RUN apt-get update \
   && apt-get install -y --no-install-recommends apt-utils 2>&1 \
   # instakk needed packages
   && apt-get install -y --no-install-recommends cmake git nano pkg-config procps libssl-dev lldb-3.9 lsb-release zlib1g-dev \
   # Install Rust components
   && rustup update \
   && rustup component add clippy rust-analysis rust-src rustfmt rls \
   #
   # Clean up
   && apt-get autoremove -y \
   && apt-get clean -y \
   && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create a user
RUN useradd -ms /bin/bash user
WORKDIR /home/user
ENV HOME /home/user
ENV USER user

# Add external scripts
ADD . /home/user
RUN chmod +x add_useful_aliases.sh

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=dialog

# Now act as `user`
USER user

# Add useful aliases
RUN ./add_useful_aliases.sh

# Install coverage tool
RUN cargo install cargo-tarpaulin

# Install http server
RUN cargo install https

# Install watcher
RUN cargo install cargo-watch
